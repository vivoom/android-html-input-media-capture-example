package co.vivoom.htmlinputmediacapture.example;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author junior.buckeridge@govivoom.com
 */
public class MainActivity extends AppCompatActivity {

  private static final String HTML_FILE = "file:///android_asset/html5-input.html";
  private static final int REQUEST_PICK_FILE_API_21 = 1;
  private static final String LOG_TAG = "HTML5 Media Capture";

  private ValueCallback<Uri[]> valueCallback;
  private WebView webView;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    webView = new WebView(this);
    webView.setWebChromeClient(new WebChromeClient() {
      @TargetApi(Build.VERSION_CODES.LOLLIPOP)
      @Override
      public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
        valueCallback = filePathCallback;
        pickFileApi21AndAbove(fileChooserParams);
        return true;
      }
    });

    if (savedInstanceState != null) {
      webView.restoreState(savedInstanceState);
    } else {
      webView.loadUrl(HTML_FILE);
    }
    setContentView(webView);
  }

  /**
   * Requests an external application to pick a file
   *
   * Note: A more involved approach could call {@linkplain #getFileSelectionIntent(CharSequence,
   * WebChromeClient.FileChooserParams, boolean)} with {@code ignoreMimeTypesForCapture = false},
   * and catch {@linkplain android.content.ActivityNotFoundException}. If the exception is thrown,
   * then the caller method could try with {@code ignoreMimeTypesForCapture = true}.
   *
   * @param fileChooserParams The params passed into {@link WebChromeClient#onShowFileChooser
   *                          WebChromeClient.onShowFileChooser}
   */
  private void pickFileApi21AndAbove(WebChromeClient.FileChooserParams fileChooserParams) {
    Intent intent = getFileSelectionIntent(getString(R.string.intent_chooser_title), fileChooserParams, true);
    startActivityForResult(intent, REQUEST_PICK_FILE_API_21);
  }

  /**
   * Returns an intent with additional information to provide a more functional
   * file capture using the camera hardware.
   *
   * @see <a href="https://www.w3.org/TR/html-media-capture/#the-capture-attribute">
   *     The Capture Attribute</a>
   *
   * @param title The title for the file chooser dialog
   * @param fileChooserParams The params passed into {@link WebChromeClient#onShowFileChooser
   *                          WebChromeClient.onShowFileChooser}
   * @param ignoreMimeTypesForCapture Whether or not to ignore the MIME type when image/video
   *                                  capture is requested from an external application.
   *                                  Since the most common image/video capture applications don't
   *                                  allow to specify the recording format, setting the MIME type
   *                                  on the intent can lead to an ActivityNotFoundException. The
   *                                  most common encoding is H.264 in an MP4 container for video;
   *                                  and JPEG for images. Here we consider the trade-off
   *                                  regarding functionality vs. accuracy. If the web is
   *                                  expecting a specific image/video format, then it would be
   *                                  good to consider setting the intent type. On the other hand,
   *                                  in many cases, the input tag is used to pick a file based on
   *                                  the general type (i.e.: image, video) and not based on the
   *                                  specific file encoding or extension.
   * @return The intent to broadcast to pick the file
   */
  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  private Intent getFileSelectionIntent(final CharSequence title,
                                        final WebChromeClient.FileChooserParams fileChooserParams,
                                        final boolean ignoreMimeTypesForCapture) {
    final List<Intent> intents = new ArrayList<>();
    final String[] acceptTypes = fileChooserParams.getAcceptTypes();
    final boolean canCapture = fileChooserParams.getMode() == WebChromeClient.FileChooserParams.MODE_OPEN
        && acceptTypes.length > 0;
    if (canCapture) {
      for (String acceptType : acceptTypes) {
        if (acceptType.startsWith("image/")) {
          Intent imageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
          if (!ignoreMimeTypesForCapture) {
            imageIntent.setTypeAndNormalize(acceptType);
          }
          intents.add(imageIntent);
        } else if (acceptType.startsWith("video/")) {
          Intent videoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
          if (!ignoreMimeTypesForCapture) {
            videoIntent.setTypeAndNormalize(acceptType);
          }
          intents.add(videoIntent);
        }
        // consider handling additional MIME types
      }
    }

    final Intent result;
    if (intents.isEmpty()) {
      result = fileChooserParams.createIntent();
    } else {
      final Intent defaultIntent;
      final boolean discardFileChooserIntent = fileChooserParams.isCaptureEnabled()
          && canCapture
          && !intents.isEmpty();
      if (discardFileChooserIntent) {
        if (intents.size() == 1) {
          result = intents.get(0);
        } else {
          defaultIntent = intents.get(0);
          intents.remove(0);
          result = getChooserIntent(title, defaultIntent, intents);
        }

      } else {
        defaultIntent = fileChooserParams.createIntent();
        result = getChooserIntent(title, defaultIntent, intents);
      }
    }

    return result;
  }

  private Intent getChooserIntent(CharSequence title, Intent defaultIntent, List<Intent> intents) {
    final Intent chooserIntent = Intent.createChooser(defaultIntent, title);
    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intents.toArray(new Intent[]{}));
    return chooserIntent;
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    webView.saveState(outState);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    super.onActivityResult(requestCode, resultCode, intent);
    if (requestCode == REQUEST_PICK_FILE_API_21) {
      if (valueCallback != null) {
        if (resultCode == Activity.RESULT_OK) {
          logIntentData(intent);
          final Uri[] result = parseResultApi21(intent);
          if (result == null) {
            valueCallback.onReceiveValue(new Uri[]{intent.getData()});
          } else {
            valueCallback.onReceiveValue(result);
          }
        } else {
          valueCallback.onReceiveValue(null);
        }
      }
    }
  }

  private void logIntentData(Intent intent) {
    Log.d(LOG_TAG, intent.getData() != null ? intent.getData().toString() : "NULL");
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  private Uri[] parseResultApi21(Intent intent) {
    return WebChromeClient.FileChooserParams.parseResult(REQUEST_PICK_FILE_API_21, intent);
  }
}
