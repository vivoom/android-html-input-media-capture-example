# HTML5 FILE INPUT CAPTURE

This is an example of how to implement proper file selection and
capture within an Android webview when an HTML5 FILE INPUT tag is
found. Over the past year, other implementations have begun to default
to not respecting the HTML5 input tag parameters and simply present a
file picker. Specifically, those implementations do not respect the
accept (mime type) and capture parameters. This results in photo,
video, or audio capture not being an option.

This implementation provides a simple drop-in solution for valid HTML5
file input tag handling.
